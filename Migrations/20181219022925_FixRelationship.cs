﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace min.Migrations
{
    public partial class FixRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BrightnessType",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BrightnessType", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Color",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Color", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TraceColor",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TraceColor", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Mineral",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BrightnessType = table.Column<int>(nullable: true),
                    cleavage = table.Column<string>(nullable: true),
                    density = table.Column<float>(nullable: false),
                    densityText = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    isFuse = table.Column<bool>(nullable: false),
                    isLiquid = table.Column<bool>(nullable: false),
                    isMagnetic = table.Column<bool>(nullable: false),
                    isMetalic = table.Column<bool>(nullable: false),
                    mainColor = table.Column<string>(nullable: true),
                    mainTrace = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    reageHcl = table.Column<bool>(nullable: false),
                    toughness = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mineral", x => x.id);
                    table.ForeignKey(
                        name: "FK_Mineral_BrightnessType_BrightnessType",
                        column: x => x.BrightnessType,
                        principalTable: "BrightnessType",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Mineral_BrightnessType",
                table: "Mineral",
                column: "BrightnessType");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Color");

            migrationBuilder.DropTable(
                name: "Mineral");

            migrationBuilder.DropTable(
                name: "TraceColor");

            migrationBuilder.DropTable(
                name: "BrightnessType");
        }
    }
}
