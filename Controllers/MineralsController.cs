﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using min.Models;
using min.Domain;

namespace min.Controllers
{
    public class MineralsController : Controller
    {
        private IMineralDomain mineralDomain;
        public MineralsController(MineralDomain mineralDomain)
        {
            this.mineralDomain = mineralDomain;
        }

        [Route("mineral")]
        [HttpGet]
        public IActionResult GetAll()
        {
            return mineralDomain.GetAll();
        }

        [Route("mineral/{id}")]
        [HttpGet]
        public IActionResult GetById(int id)
        {
            var item = mineralDomain.GetById(id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [Route("search/{name}")]
        [HttpGet]
        public IActionResult GetByName(string name)
        {
            var item = mineralDomain.GetByName(name);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [Route("mineral")]
        [HttpPost]
        public IActionResult PostMineral([FromBody]Mineral mineral)
        {
            if (mineral == null)
            {
                return BadRequest();
            }
            return mineralDomain.PostMineral(mineral);
        }

        [Route("mineral/{id}")]
        [HttpPut]
        public IActionResult PutMineral([FromBody]Mineral mineral, int id)
        {
            if (mineral == null)
            {
                return BadRequest();
            }
            var item = mineralDomain.PutMineral(mineral, id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [Route("mineral/{id}")]
        [HttpDelete]
        public IActionResult DeleteMineral(int id)
        {
            var item = mineralDomain.DeleteMineral(id);
            if (item == null)
            {
                return NotFound();
            }
            else
            {
                return item;
            }
        }

        [Route("identificarion/{type}")]
        [HttpPost]
        public IActionResult Identificarion([FromBody] Mineral mineral, int type)
        {
            var requestData = new RequestData();
            requestData.mineral = mineral;
            requestData.sequence = type;

            var minerals = mineralDomain.Identification(requestData, type);
            return new ObjectResult(minerals);
        }

        [Route("keepAlive")]
        [HttpGet]
        public IActionResult KeepAlive()
        {
            return new ObjectResult(true);
        }
    }
}
