﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;

namespace min.Models
{
    public class RequestData
    {
        public Mineral mineral { get; set; }
        public string guid { get; set; }
        public int sequence { get; set; }
    }
}