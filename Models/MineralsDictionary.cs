﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System;

namespace min.Models
{
    public class MineralsDictionary
    {
        public Dictionary<string, List<Mineral>> mineralsLocalList = new Dictionary<string, List<Mineral>>();
    }
}