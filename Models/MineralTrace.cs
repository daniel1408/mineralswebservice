﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace min.Models
{
    [Table("MineralTrace")]
    public class MineralTrace
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("TraceColor")]
        public int TraceColorId { get; set; }
        [ForeignKey("Mineral")]
        public int MineralId { get; set; }
    }
}