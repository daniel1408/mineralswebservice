﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Linq;

namespace min.Models
{
    [Table("Mineral")]
    public class Mineral
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [Column("name")]
        public string name { get; set; }
        [Column("density")]
        public float density { get; set; }
        [Column("densityText")]
        public string densityText { get; set; }
        [Column("toughness")]
        public float toughness { get; set; }
        [Column("description")]
        public string description { get; set; }
        [Column("isMetalic")]
        public bool isMetalic { get; set; }
        [Column("isLiquid")]
        public bool isLiquid { get; set; }
        [Column("isFuse")]
        public bool isFuse { get; set; }
        [Column("isMagnetic")]
        public bool isMagnetic { get; set; }
        [Column("reageHcl")]
        public bool reageHcl { get; set; }
        [Column("cleavage")]
        public string cleavage { get; set; }
        [Column("mainColor")]
        public string mainColor { get; set; }
        [Column("mainTrace")]
        public string mainTrace { get; set; }
        [ForeignKey("BrightnessType")]
        public BrightnessType brightnessType { get; set; }

        public List<Color> colorList;
        public List<TraceColor> traceList;
    }
}