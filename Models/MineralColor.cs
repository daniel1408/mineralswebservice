﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace min.Models
{
    [Table("MineralColor")]
    public class MineralColor
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        [ForeignKey("Color")]
        public int ColorId { get; set; }
        [ForeignKey("Mineral")]
        public int MineralId { get; set; }
    }
}