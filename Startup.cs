﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using min.Controllers;
using min.Models;
using min.Domain;

namespace min
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MineralsContext>(
                opt => opt.UseSqlServer(""));               
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info() {Title = "Minerals", Version = "v1"});
            });

            services.AddScoped<MineralsContext>();
            services.AddSingleton<IMineralDomain, MineralDomain>();
            services.AddMvc();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseSwagger();

            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Minerals");
            });

            app.UseCors(builder => builder.WithOrigins("http://localhost:4200"));
            app.UseMvc();
        }
    }
}