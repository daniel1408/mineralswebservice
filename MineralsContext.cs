﻿using Microsoft.EntityFrameworkCore;
using min.Models;

namespace min
{
    public class MineralsContext : DbContext
    {
        public MineralsContext(
            DbContextOptions<MineralsContext> options) : base(options)
        { }
        public DbSet<Mineral> Minerals { get; set; }
        public DbSet<Color> colorList { get; set; }
        public DbSet<TraceColor> traceColorList { get; set; }
        public DbSet<MineralColor> mineralColor { get; set; }
        public DbSet<MineralTrace> mineralTrace { get; set; }
    }
}