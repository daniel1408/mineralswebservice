using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using min.Models;

namespace min.Domain
{
    public interface IMineralDomain
    {
        IActionResult PostMineral(Mineral mineral);
        IActionResult PutMineral(Mineral mineral, int id);
        IActionResult GetAll();
        IActionResult GetByName(string name);
        IActionResult GetById(int id);
        IActionResult DeleteMineral(int id);
        IActionResult Identification(RequestData mineral, int type);
    }
}


