using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using min.Domain;
using min.Models;
using Newtonsoft.Json;

namespace min.Domain
{
    public class MineralDomain : IMineralDomain
    {
        private MineralsContext _context;
        private MineralsDictionary cache;
        public MineralDomain(MineralsContext context, MineralsDictionary cache)
        {
            _context = context;
            this.cache = cache;
        }
        public IActionResult DeleteMineral(int id)
        {
            var item = _context.Set<Mineral>().AsQueryable().Where((x) => x.id == id);
            if (item != null)
            {
                _context.Minerals.Remove(item.FirstOrDefault());
                _context.SaveChanges();
            }
            return new ObjectResult(item);
        }

        public IActionResult GetAll()
        {
            var mineral = _context.Set<Mineral>().AsQueryable();
            var colorRelationship = _context.Set<MineralColor>().AsQueryable();
            var color = _context.Set<Color>().AsQueryable();

            var query = from min in mineral
                        orderby min.name
                        select new
                        {
                            id = min.id,
                            name = min.name,
                            density = min.density,
                            densityText = min.densityText,
                            toughness = min.toughness,
                            description = min.description,
                            isMetalic = min.isMetalic,
                            isLiquid = min.isLiquid,
                            isFuse = min.isFuse,
                            isMagnetic = min.isMagnetic,
                            reageHcl = min.reageHcl,
                            cleavage = min.cleavage,
                            mainColor = min.mainColor,
                            mainTrace = min.mainTrace,
                            brightnessType = min.brightnessType,
                        };
            return new ObjectResult(query.ToList());
        }

        public IActionResult GetById(int id)
        {
            var mineral = _context.Set<Mineral>().AsQueryable();
            var query = from min in mineral
                        where min.id == id
                        select new Mineral
                        {
                            name = min.name,
                            density = min.density,
                            densityText = min.densityText,
                            toughness = min.toughness,
                            description = min.description,
                            isMetalic = min.isMetalic,
                            isLiquid = min.isLiquid,
                            isFuse = min.isFuse,
                            isMagnetic = min.isMagnetic,
                            reageHcl = min.reageHcl,
                            cleavage = min.cleavage,
                            mainColor = min.mainColor,
                            mainTrace = min.mainTrace,
                            brightnessType = min.brightnessType,
                        };

            if (query != null)
                return new ObjectResult(query.FirstOrDefault());
            else
                return null;
        }

        public IActionResult GetByName(string name)
        {
            var parameters = name.ToUpper();
            var minerals = _context.Set<Mineral>().AsQueryable();
            var query = from min in minerals
                        where min.name == parameters
                        select new
                        {
                            name = min.name,
                            density = min.density,
                            densityText = min.densityText,
                            toughness = min.toughness,
                            description = min.description,
                            mainColor = min.mainColor,
                            mainTrace = min.mainTrace,
                        };

            if (query != null)
            {
                return new ObjectResult(query.FirstOrDefault());
            }
            else
            {
                return null;
            }
        }
        public IActionResult PostMineral(Mineral mineral)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    _context.Minerals.Add(mineral);
                    _context.SaveChanges();
                    transaction.Commit();
                    return new ObjectResult(mineral);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }
        public IActionResult PutMineral(Mineral mineral, int id)
        {
            var item = _context.Set<Mineral>().AsQueryable().Where((x) => x.id == id);

            if (item != null)
            {
                using (var transaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        _context.Minerals.Update(mineral);
                        _context.SaveChanges();
                        transaction.Commit();
                        return new ObjectResult(mineral);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        public IActionResult Identification([FromBody] RequestData requestData, int type)
        {
            var mineral = requestData.mineral;
            var sequence = requestData.sequence;
            var mineralResponseList = new List<Mineral>();

            if (requestData.sequence == 1)
            {
                var minerals = _context.Set<Mineral>().AsQueryable();
                var relationshipCol = _context.Set<MineralColor>().AsQueryable();
                var relationshipTrac = _context.Set<MineralTrace>().AsQueryable();
                var colors = _context.Set<Color>().AsQueryable();
                var traces = _context.Set<TraceColor>().AsQueryable();

                var query = from min in minerals
                            where min.isLiquid == mineral.isLiquid
                            select new Mineral
                            {
                                name = min.name,
                                density = min.density,
                                densityText = min.densityText,
                                toughness = min.toughness,
                                description = min.description,
                                isMetalic = min.isMetalic,
                                isLiquid = min.isLiquid,
                                isFuse = min.isFuse,
                                isMagnetic = min.isMagnetic,
                                reageHcl = min.reageHcl,
                                cleavage = min.cleavage,
                                mainColor = min.mainColor,
                                mainTrace = min.mainTrace,
                                brightnessType = min.brightnessType,
                                colorList = (from col in colors
                                             join relC in relationshipCol on col.id equals relC.ColorId
                                             where relC.MineralId == min.id
                                             select new Color
                                             {
                                                 id = col.id,
                                                 name = col.name,
                                             }).ToList(),

                                traceList = (from trac in traces
                                             join relT in relationshipTrac on trac.id equals relT.TraceColorId
                                             where relT.MineralId == min.id
                                             select new TraceColor
                                             {
                                                 id = trac.id,
                                                 name = trac.name,
                                             }).ToList()
                            };

                if (query.ToList().Count > 1)
                {
                    this.cache.mineralsLocalList.Remove(requestData.guid);
                    this.cache.mineralsLocalList.Add(requestData.guid, query.ToList());
                }
                else
                {
                    this.cache.mineralsLocalList.Remove(requestData.guid);
                }
                return new ObjectResult(query.ToList());
            }
            else if (requestData.sequence == 2)
            {
                var minerals = this.cache.mineralsLocalList.Where(x => x.Key == requestData.guid).FirstOrDefault();
                mineralResponseList = minerals.Value.FindAll((x) => x.isMetalic == mineral.isMetalic && x.toughness <= mineral.toughness + 0.9 && x.toughness >= mineral.toughness - 0.9);

                if (mineralResponseList.Count > 1)
                {
                    this.cache.mineralsLocalList.Remove(requestData.guid);
                    this.cache.mineralsLocalList.Add(requestData.guid, mineralResponseList.ToList());
                }
                else
                {
                    this.cache.mineralsLocalList.Remove(requestData.guid);
                }
            }
            else if (requestData.sequence == 3)
            {
                var minerals = this.cache.mineralsLocalList.Where(x => x.Key == requestData.guid).FirstOrDefault();
                mineralResponseList = minerals.Value.FindAll((x) => x.densityText == mineral.densityText);

                if (mineralResponseList.Count > 1)
                {
                    this.cache.mineralsLocalList.Remove(requestData.guid);
                    this.cache.mineralsLocalList.Add(requestData.guid, mineralResponseList.ToList());
                }
                else
                {
                    this.cache.mineralsLocalList.Remove(requestData.guid);
                }
            }
            else if (requestData.sequence == 4)
            {
                var minerals = this.cache.mineralsLocalList.Where(x => x.Key == requestData.guid).FirstOrDefault();
                mineralResponseList = minerals.Value.FindAll((x) => x.colorList.Any(y => y.name == mineral.mainColor));

                if (mineralResponseList.Count > 1)
                {
                    this.cache.mineralsLocalList.Remove(requestData.guid);
                    this.cache.mineralsLocalList.Add(requestData.guid, mineralResponseList.ToList());
                }
                else
                {
                    this.cache.mineralsLocalList.Remove(requestData.guid);
                }
            }
            else if (requestData.sequence == 5)
            {
                var minerals = this.cache.mineralsLocalList.Where(x => x.Key == requestData.guid).FirstOrDefault();
                mineralResponseList = minerals.Value.FindAll((x) => x.traceList.Any(y => y.name == mineral.mainTrace));

                this.cache.mineralsLocalList.Remove(requestData.guid);
            }
            _context.SaveChanges();
            return new ObjectResult(mineralResponseList.ToList());
        }

    }
}



